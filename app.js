const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const { middleware } = require("@magento/upward-js");
async function serve() {
  dotenv.config();

  //middleweare to parse all the data going to database + always before routes
  app.use(cors());
  app.use(bodyParser.json({ extended: false }));

  //connect to database

  const promise = mongoose.connect(
    process.env.DB_CONNECT,
    { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true },
    () => console.log("connected to database")
  );

  //import routes

  const products = require("./routes/products");
  const categories = require("./routes/categories");
  const paths = require("./routes/paths");

  app.use("/products", products);
  app.use("/categories", categories);
  app.use("/paths", paths);

  //@route GET /
  //@desc Main app route
  app.get("/", (req, res) => {
    let today = new Date();
    res.send(`hello from test service! ${today}`);
  });

  const upwardMiddleware = await middleware(`${__dirname}/upward-test.yml`, {
    MAGENTO_GRAPHQL_URL: "https://migration.lepape.com/graphql",
  });

  app.use(upwardMiddleware);

  //start listening to the server
  const port = 5000;
  app.listen(port, () => console.log(`Posts Service started on port ${port}`));
}

serve();
