const express = require("express");
const router = express.Router();
const Product = require("../models/Product");

router.get("/", async (req, res) => {
  try {
    let products = await Product.find();
    res.json(products);
  } catch (err) {
    res.json({ message: err });
  }
});

router.post("/", async (req, res) => {
  const product = new Product({
    name: req.body.name,
    sku: req.body.sku,
    price: req.body.price,
    stock: req.body.stock,
    sizes: req.body.sizes,
    description: req.body.description,
    img: req.body.img,
    url: req.body.url,
  });
  try {
    let savedProduct = await product;
    savedProduct.save();
    res.json(savedProduct);
  } catch (err) {
    res.json({ message: err });
  }
});

router.get("/:product", async (req, res) => {
  try {
    let product = await Product.findOne({ url: req.params.product });
    res.json(product);
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
