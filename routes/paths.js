const express = require("express");
const router = express.Router();
const Path = require("../models/Path");

router.get("/", async (req, res) => {
  try {
    let paths = await Path.find();
    res.json(paths);
  } catch (err) {
    res.json({ message: err });
  }
});

router.post("/", async (req, res) => {
  const path = new Path({
    path: req.body.path,
    type: req.body.type,
  });
  try {
    let savedPath = await path;
    savedPath.save();
    res.json(savedPath);
  } catch (err) {
    res.json({ message: err });
  }
});

router.get("/:path", async (req, res) => {
  try {
    let path = await Path.findOne({ path: req.params.path });
    if (path === null) {
      res.json("route not found!");
    } else {
      res.json(path);
    }
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
