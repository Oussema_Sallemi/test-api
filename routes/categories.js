const express = require("express");
const router = express.Router();
const Category = require("../models/Category");

router.get("/", async (req, res) => {
  try {
    let categories = await Category.find();
    res.json(categories);
  } catch (err) {
    res.json({ message: err });
  }
});

router.post("/", async (req, res) => {
  const category = new Category({
    name: req.body.name,
    is_active: req.body.is_active,
    level: req.body.level,
    include_in_menu: req.body.include_in_menu,
    description: req.body.description,
    url: req.body.url,
  });
  try {
    let savedCategory = await category;
    savedCategory.save();
    res.json(savedCategory);
  } catch (err) {
    res.json({ message: err });
  }
});

router.get("/:category", async (req, res) => {
  try {
    let category = await Category.findOne({ url: req.params.category });
    res.json(category);
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
