const mongoose = require("mongoose");

const PathSchema = mongoose.Schema({
  path: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Path", PathSchema);
